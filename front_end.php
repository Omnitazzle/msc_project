<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<meta name="viewport" content="width=device-width">
<meta http-equiv="content-language" content="en" />
 <link href="front_end_style.css" rel="stylesheet"/>
     
     <script type="text/javascript">
	 
	 function populateMenus(){
		 menu1 = document.getElementById("range1");
		 menu2 = document.getElementById("range2");
		 menu3 = document.getElementById("docRange");
		 menu4 = document.getElementById("hskLevel");
		 menu5 = document.getElementById("userTopics");
		 
		 option=null;
		 number=null;
		 for (var i=0; i <= 1500; i += 100){
			 option = document.createElement("option");
			 number = i;
			 option.value=number;
			 option.innerHTML=number;
			 menu1.appendChild(option);
			 }
			 
		for(var i=100; i<= 3000; i += 100) {
			option = document.createElement("option");
			 number = i;
			 option.value=number;
			 option.innerHTML=number;
			 menu2.appendChild(option);
		}
		
		for(var i=10; i<=50; i+=10){
			option = document.createElement("option");
			 number = i;
			 option.value=number;
			 option.innerHTML=number;
			 menu3.appendChild(option);
		}
			
			for(var i=1; i<=6; i+=1){
			option = document.createElement("option");
			 number = i;
			 option.value= "HSK" + number;
			 option.innerHTML= "HSK" + number;
			 menu4.appendChild(option);
		}
		
			for(var i=1; i<=6; i+=1){
			option = document.createElement("option");
			 number = i;
			 option.value= "Topics" + number;
			 option.innerHTML= "Topics" + number;
			 menu5.appendChild(option);
		}
		
		
		
			
	 
	 }
	 
	 </script>
		 
	 
	
     
</head>

<body onload ="populateMenus()" id="site">


    
      

<div class="middle">
     <center>
       <p> 欢迎 - Welcome</p>
       <br />
       <p> Choose text length</p>
       
       <form  accept-charset="utf-8" action="http://localhost:8888/finalproject.php" method="post"> 
       		<label>from
       			<select id="range1" name="range1"></select>
       		<label> to
       			<select id="range2" name="range2"></select>
        <br /> <br />    
				<label> No. of documents to be returned
            	<select id="docRange" name="docRange"></select>
        <br /> <br />
        		<label> HSK Level
             	<select id="hskLevel" name = "hskLevel"></select> 
         <br /> <br />       	
         		<label> Topic List
              	<select id="userTopics" name="userTopics"></select>
                 
          <p><input type="submit" name="view_all" value="View Results"></p>
  
        </form>
      </center>   
</div>
  
 

<?php

#if button is clicked
if(isset($_POST['view_all'])) {	
	
	$range1 = $_POST['range1'];
	$range2 = $_POST['range2'];
	$docRange = $_POST['docRange'];
	$hskLevel = $_POST['hskLevel'];
	$userTopics = $_POST['userTopics'];
			
?>	


<table id="table" border="1;" class = "table" >		
    
    

<?php 

	#execute python script
	exec(" /Users/$USER/anaconda/bin/python /Applications/MAMP/cgi-bin/back_end.py $range1 $range2 $docRange $hskLevel $userTopics" , $output );	
	

	
	#convert strings to arrays
	array_walk($output, function(&$val) {$val = explode('&&&', $val);});
				
	#get finalscore for sorting
	foreach($output as $value => $row){
	$count[$value] = $row[4];
	}
				
			
	#sort by finalscore descending
	array_multisort($count, SORT_DESC, $output);
		

		
	foreach($output as $myArray) {
			
?>



<tr>
	<td>
	<div class="left">
		<div class="result-text">Similarity<br />Percentage</div>
    	<!-- Print percentage -->
    	<span class="result-score"><?php echo $myArray[0]; ?>% </span>
    	<br />
    	<div class="common-text"> <br /> Known <br /> characters  </div>
    	<!-- Print no. of known characters -->
     	<span class="common-score"> <?php echo $myArray[3];?> out of <?php echo $myArray[5];?> </span>
    	 <br />
    </div> 
    		 

    <div class="wrapper">
        <span >
        <!-- Print text snippet -->
        <strong class='doc'><?php echo $myArray [2]; ?>... </strong>
        </span>   
        <br />
        <hr>
        <p class="description">Click below to continue reading</p>     
		<!-- Link to read full text -->              
        <h3 class="link"><a href="http://localhost:8983/solr/collection2/select?q=id%3A<?php echo $myArray[1] ?>&wt=csv&indent=true" target="_blank" class="result-linkin">Read full text</a> &raquo;</h3> 
    </div>
            
 
<?php  

		}

?>
         

  </td>
                
</tr>
      
</table>
                
                
 
<?php
 
  }  
  
?>
 
		

</body>
</html>

















